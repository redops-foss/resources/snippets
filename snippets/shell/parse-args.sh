#!/usr/bin/env bash

# shellcheck disable=SC1090
[ "$(uname -s)" = "Darwin" ] && __i="$HOME/Library/Caches" || __i="$HOME/.cache" && __i="${IMPORT_CACHE:-${XDG_CACHE_HOME:-${LOCALAPPDATA:-${__i}}}/import.pw}/import" && [ -r "$__i" ] || curl -sfLSo "$__i" --create-dirs https://import.pw && . "$__i" && unset __i

import "kward/shflags"

#### OR
