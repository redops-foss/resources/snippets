#!/usr/bin/env bash

get_real_path() {
  path=$(readlink -f "$1")

  if [ -x "$(command -v wslpath)" ]; then
    wslpath -m "$path"
    return $?
  fi

  if [ -x "$(command -v cygpath)" ]; then
    cygpath -m "$path"
    return $?
  fi

  echo "$path"
  return $?
}
