#!/usr/bin/env sh

# shellcheck disable=SC1090
. "$(command -v import)"

import "https://gitlab.com/mlfbvr/mlfbvr/-/raw/1.0.1/snippets/shell/get_real_path.sh"

script_dir="$(dirname "$(readlink -f "$0")")"

play_dir="$script_dir"/__mockdata__/results
under_test_script="$script_dir"/optimize.sh

setUp() {
  rm -rf "$play_dir"
  cp -r "$script_dir"/__mockdata__/input "$play_dir"
}

tearDown() {
  rm -rf "$play_dir"
}

testShouldOptimizeSvgs() {
  bash "$under_test_script" "$play_dir"

  assertEquals "$(cat "$script_dir"/__mockdata__/expected/1-venus.svg)" "$(cat "$play_dir"/1-venus.svg)"
  # shellcheck disable=SC2012
  assertEquals 5 "$(ls -1 "$play_dir" | wc -l)"
}

import "kward/shunit2"
