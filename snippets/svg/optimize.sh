#!/usr/bin/env bash

main() {
  local root=${1:-$(pwd)}

  for file in "$root"/*.svg; do
    inkscape --export-area-drawing --export-filename="$file" --export-plain-svg "$file" >/dev/null 2>&1
  done

  npx --quiet svgo --disable=removeViewBox --enable=removeDimensions "$root"/*.svg >/dev/null
}

main "$@"
