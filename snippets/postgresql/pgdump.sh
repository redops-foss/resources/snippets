#!/bin/env bash

# shellcheck disable=SC1090
[ "$(uname -s)" = "Darwin" ] && __i="$HOME/Library/Caches" || __i="$HOME/.cache" && __i="${IMPORT_CACHE:-${XDG_CACHE_HOME:-${LOCALAPPDATA:-${__i}}}/import.pw}/import" && [ -r "$__i" ] || curl -sfLSo "$__i" --create-dirs https://import.pw && . "$__i" && unset __i

import "kward/shflags"
import "https://gitlab.com/mlfbvr/mlfbvr/-/raw/1.0.1/snippets/shell/get_real_path.sh"

# Archive, recreate & restore
main() {
  local username=$1
  local password=$2
  local dbname=$3
  local host=$4
  local port=$5
  local dump_file=$6

  export PGPASSWORD=$password
  export PGHOST=$host
  export PGPORT=$port

  NOW=$(date +"%Y%m%d%H%I%S")
  TEMP_FILE=$(mktemp -d -t pgdump-XXXXXXXX)/"${dbname}.${NOW}.sql.gz"

  # Dump old one
  pg_dump -U "$username" -O -x "$dbname" | gzip -9 > "$TEMP_FILE"
  echo "In case of errors, your old database dump is here : $(get_real_path "$TEMP_FILE")"

  # Drop and recreate
  psql -U "$username" postgres > /dev/null <<EOF
SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '$dbname';
DROP DATABASE $dbname;
CREATE DATABASE $dbname;
EOF

  # Restore
  gzip -f -d -k "$dump_file"
  psql -U "$username" -f "${dump_file/.gz/}" "$dbname" > /dev/null

  # Clean
  rm -rf "${dump_file/.gz/}"
  export PGPASSWORD=
  export PGHOST=
  export PGPORT=
}

# Declare & parse args
# configure shflags
DEFINE_string 'username' '' 'DB username' 'u'
DEFINE_string 'password' '' 'DB password' 'p'
DEFINE_string 'dbname' '' 'DB name' 'd'
DEFINE_string 'host' 'localhost' 'DB host' ''
DEFINE_string 'port' '5432' 'DB port' ''

# shellcheck disable=SC2034
FLAGS_HELP="USAGE: $0 [flags] gz_dump_file"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# check for dump_file
if [ $# -eq 0 ]; then
  echo 'error: gz_dump_file missing' >&2
  flags_help
  exit 1
fi

# shellcheck disable=SC2154
main "$FLAGS_username" "$FLAGS_password" "$FLAGS_dbname" "$FLAGS_host" "$FLAGS_port" "$1"

exit 0
