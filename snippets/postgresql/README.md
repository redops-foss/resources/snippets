# Postgres snippets:

1. `pgdump.sh [flags] dump_file.sql.gz`:

   Dump current database, drop it and restore it using a `.sql.gz` file

   You need to have `psql` and `pg_dump` in your Path beforehand.
