#!/usr/bin/env sh

# shellcheck disable=SC1090
. "$(command -v import)"

script_dir="$(dirname "$(readlink -f "$0")")"
export DUMP_PARAMS="-u $POSTGRES_USER -p $POSTGRES_PASSWORD --host $POSTGRES_HOST -d $POSTGRES_DB"
export DUMP_PARAMS_LONG="--user $POSTGRES_USER --password $POSTGRES_PASSWORD --host $POSTGRES_HOST --dbname $POSTGRES_DB"

testFailIfNoDumpFileIsProvided() {
  bash "$script_dir"/pgdump.sh > /dev/null 2>&1
  assertTrue "[ $? -ne 0 ]"
}

testFailIfNoDumpFileIsProvidedShortParams() {
  # shellcheck disable=SC2086
  bash "$script_dir"/pgdump.sh $DUMP_PARAMS > /dev/null 2>&1
  assertTrue "[ $? -ne 0 ]"
}

testFailIfNoDumpFileIsProvidedLongParams() {
  # shellcheck disable=SC2086
  bash "$script_dir"/pgdump.sh $DUMP_PARAMS_LONG > /dev/null 2>&1
  assertTrue "[ $? -ne 0 ]"
}

testShouldUploadDumpFileShortParams() {
  DUMP_FILE="$script_dir"/__mockdata__/movies.sql.gz
  # shellcheck disable=SC2086
  bash "$script_dir"/pgdump.sh $DUMP_PARAMS "$DUMP_FILE" > /dev/null
  assertTrue "[ $? -eq 0 ]"

  export PGPASSWORD=$POSTGRES_PASSWORD
  export PGHOST=$POSTGRES_HOST

  first_name=$(psql -AXt -U "$POSTGRES_USER" -c "SELECT first_name FROM actor where actor_id = 1;" "$POSTGRES_DB")

  assertTrue "[ $? -eq 0 ]"
  assertEquals "Penelope" "$first_name"
}

testShouldUploadDumpFileLongParams() {
  DUMP_FILE="$script_dir"/__mockdata__/movies.sql.gz
  # shellcheck disable=SC2086
  bash "$script_dir"/pgdump.sh $DUMP_PARAMS_LONG "$DUMP_FILE" > /dev/null
  assertTrue "[ $? -eq 0 ]"

  export PGPASSWORD=$POSTGRES_PASSWORD
  export PGHOST=$POSTGRES_HOST

  first_name=$(psql -AXt -U "$POSTGRES_USER" -c "SELECT first_name FROM actor where actor_id = 1;" "$POSTGRES_DB")

  assertTrue "[ $? -eq 0 ]"
  assertEquals "Penelope" "$first_name"
}

import "kward/shunit2"
