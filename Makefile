.PHONY: lint test clean dc_down dc_lint_shell dc_lint_docker dc_test_shell

lint: clean dc_lint_shell dc_lint_docker clean
test: clean dc_test_shell clean

clean: dc_down

dc_down:
	docker-compose down --remove-orphans && docker-compose rm -f
dc_lint_shell:
	docker-compose up shell.lint
dc_lint_docker:
	docker-compose up docker.lint
dc_test_shell:
	docker-compose up --build shell.test
