FROM ubuntu:20.10

RUN apt-get update > /dev/null \
  && apt-get install -y --no-install-recommends \
     ca-certificates=20200601 \
     curl=7.68.0-* \
     inkscape=1.0-* \
     nodejs=10.19.0~* \
     npm=6.14.7+* \
     postgresql-client-12=12.3-* > /dev/null \
  && rm -rf /var/lib/apt/lists/* \
  && curl -sfLS https://import.pw > /usr/local/bin/import \
  && chmod +x /usr/local/bin/import \
  && mkdir -p /data

WORKDIR /data

CMD ["bash", "-c", "find /data -name '*.spec.sh' -exec sh -c 'for n; do sh \"$n\" || exit 1; done;' sh {} +"]
